package com.bot.util;

import com.bot.model.Message;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MessageParser{

    private String text;
    private String fromId;
    private String type;
    private static final Logger logger = LogManager.getLogger();

    public Message Parser(String incomingJson){
        Message message;
        JsonParser parser = new JsonParser();
        JsonElement jsonElement = parser.parse(incomingJson);
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        type = jsonObject.get("type").getAsString();
        if (type.equals("message_new")) {
            JsonObject rootJsonObject = jsonObject.getAsJsonObject("object");
            JsonObject messageJsonObject = rootJsonObject.getAsJsonObject("message");
            fromId = messageJsonObject.get("from_id").getAsString();
            text = messageJsonObject.get("text").getAsString();
            logger.info("type: " + type + "; from_id: " + fromId + "; text: " + text);
            message = new Message(type, fromId, text);
        } else {
            logger.info("type: " + type + "; from_id: null; text: null");
            message = new Message(type);
        }
        return message;
    }



    public String getText() {
        return text;
    }

    public String getFromId() {
        return fromId;
    }

    public String getType() {
        return type;
    }
}
