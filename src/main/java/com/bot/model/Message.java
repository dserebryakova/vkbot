package com.bot.model;

public class Message {
    private String text;
    private String fromId;
    private String type;

    public Message(String type, String fromId, String text) {
        this.fromId = fromId;
        this.text = text;
        this.type = type;
    }

    public Message(String type) {
        this.type = type;
        this.text = "";
        this.fromId = "";

    }

    public String getType() {
        return type;
    }

    public String getFromId() {
        return fromId;
    }

    public String getText() {
        return text;
    }
}
