package com.bot.controller;

import com.bot.model.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import com.bot.util.MessageGeneration;
import com.bot.util.MessageParser;

import java.util.Random;

@RestController
public class BotController {
    @Value("${ACESS_TOKEN}")
    private String ACCESS_TOKEN;
    @Value("${CALLBACK_API_CONFIRMATION_TOKEN}")
    private String CALLBACK_API_CONFIRMATION_TOKEN;

    @Autowired
    RestTemplate restTemplate;

    private static final String CALLBACK_API_EVENT_CONFIRMATION = "confirmation";
    private static final String CALLBACK_API_EVENT_MESSAGE_NEW = "message_new";

    private MessageGeneration messageGeneration;
    private MessageParser messageParser;
    private Message message;

    private final Random random = new Random();

    private static final Logger logger = LogManager.getLogger();

    private String vkApiMessageSendMethod = "https://api.vk.com/method/messages.send";

    @RequestMapping(value = "/callback", method = RequestMethod.POST, consumes = {"application/json"})
    public @ResponseBody String botResponse(@RequestBody String incomingMessage) {
        if (incomingMessage != null) {
            messageParser = new MessageParser();
            message = messageParser.Parser(incomingMessage);
            String type = message.getType();

            if (type.equals(CALLBACK_API_EVENT_CONFIRMATION)) {
                return CALLBACK_API_CONFIRMATION_TOKEN;
            } else if (type.equals(CALLBACK_API_EVENT_MESSAGE_NEW)) {
                String userId = message.getFromId();
                String message = this.message.getText();
                messageGeneration = new MessageGeneration();
                String botAnswer = buildBotResponse(userId, messageGeneration.getBotAnswer(message));
                logger.info(botAnswer);

                try {
                    restTemplate.getForObject(botAnswer, String.class);
                } catch (Exception ex) {
                    logger.error("Error message: ", ex);
                }
            }
        }
        return "ok";
    }

    private String buildBotResponse(String userId, String message) {
        return (vkApiMessageSendMethod + "?user_id=" + userId + "&message=" + message + "&random_id=" + random.nextInt()+"&access_token="+ ACCESS_TOKEN +"&v=5.120");
    }
}
